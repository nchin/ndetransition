<div class="view-fields-container grant-person">
	<?php if(isset($fields["field_grant_address_name_line"]->content)): ?>
		<div class="views-field-label">
			<?php print $fields["field_grant_address_name_line"]->label; ?>
		</div>
		<?php print $fields["field_grant_address_name_line"]->content; ?>
	<?php endif; ?>
	<?php if(isset($fields["field_grant_address_thoroughfare"]->content)): ?>
		<?php print $fields["field_grant_address_thoroughfare"]->content; ?>,
	<?php endif; ?>
	<?php if(isset($fields["field_grant_address_locality"]->content)): ?>
		<?php print $fields["field_grant_address_locality"]->content; ?>,
	<?php endif; ?>
	<?php if(isset($fields["field_grant_address_administrative_area"]->content)): ?>
		<?php print $fields["field_grant_address_administrative_area"]->content; ?>
	<?php endif; ?>
	<?php if(isset($fields["field_grant_address_postal_code"]->content)): ?>
		<?php print $fields["field_grant_address_postal_code"]->content; ?>
	<?php endif; ?>
	<?php if(isset($fields["field_grant_contact_phone"]->content)): ?>
			<?php print $fields["field_grant_contact_phone"]->content; ?>
	<?php endif; ?>
</div>

<?php if(isset($fields["field_grant_award"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["field_grant_award"]->label; ?>
		</div>
		<?php print $fields["field_grant_award"]->content; ?>
	</div>
<?php endif; ?>

<?php if(isset($fields["body"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["body"]->label; ?>
		</div>
		<?php print $fields["body"]->content; ?>
	</div>
<?php endif; ?>