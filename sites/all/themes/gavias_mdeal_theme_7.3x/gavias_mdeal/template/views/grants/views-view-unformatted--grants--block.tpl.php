<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="grants">
	<?php if (!empty($title)): ?>
	  	<div class="grant__year">
	  		<h3><?php print $title; ?></h3>
	  	</div>
	<?php endif; ?>

	<div class="grant__list-wrapper">
		<?php foreach ($rows as $id => $row): ?>
		    <?php print $row; ?>
		<?php endforeach; ?>
	</div>
</div>