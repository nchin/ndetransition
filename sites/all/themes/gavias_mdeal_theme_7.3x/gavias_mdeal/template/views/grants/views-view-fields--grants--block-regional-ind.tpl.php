<?php if(isset($fields["field_grant_esu_served"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["field_grant_esu_served"]->label; ?>
		</div>
		<?php print $fields["field_grant_esu_served"]->content; ?>
	</div>
<?php endif; ?>

<?php if(isset($fields["field_grant_team_name"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["field_grant_team_name"]->label; ?>
		</div>
		<?php print $fields["field_grant_team_name"]->content; ?>
	</div>
<?php endif; ?>

<?php if(isset($fields["field_grant_school_district"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["field_grant_school_district"]->label; ?>
		</div>
		<?php print $fields["field_grant_school_district"]->content; ?>
	</div>
<?php endif; ?>

<div class="view-fields-container grant-person">
	<?php if(isset($fields["field_grant_contact_name"]->content)): ?>
		<div class="views-field-label">
			<?php print $fields["field_grant_contact_name"]->label; ?>
		</div>
		<?php print $fields["field_grant_contact_name"]->content; ?>
	<?php endif; ?>
	<?php if(isset($fields["field_grant_contact_title"]->content)): ?>
		<div class="views-field-label">
			<?php print $fields["field_grant_contact_title"]->label; ?>
		</div>
		<?php print $fields["field_grant_contact_title"]->content; ?>
	<?php endif; ?>
	<?php if(isset($fields["field_grant_contact_address"]->content)): ?>
		<?php print $fields["field_grant_contact_address"]->content; ?>
	<?php endif; ?>
	<?php if(isset($fields["field_grant_contact_phone"]->content)): ?>
		<?php print $fields["field_grant_contact_phone"]->content; ?>
	<?php endif; ?>
</div>

<?php if(isset($fields["field_grant_amount_requested"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["field_grant_amount_requested"]->label; ?>
		</div>
		<?php print $fields["field_grant_amount_requested"]->content; ?>
	</div>
<?php endif; ?>

<?php if(isset($fields["body"]->content)): ?>
	<div class="view-fields-container">
		<div class="views-field-label">
			<?php print $fields["body"]->label; ?> for the <?php print $fields["field_grant_year"]->content; ?> school year include the following:
		</div>
		<?php print $fields["body"]->content; ?>
	</div>
<?php endif; ?>