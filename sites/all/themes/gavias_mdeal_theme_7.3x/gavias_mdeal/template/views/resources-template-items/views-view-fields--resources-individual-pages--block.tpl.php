<div class="resource-item__description">
	<?php print $fields["body"]->content; ?>
</div>

<?php if(isset($fields["field_resource_link"]->content)): ?>
	<a class="btn btn-primary" href="<?php print $fields["field_resource_link"]->content; ?>" role="button">Visit <?php print $fields["title"]->content; ?> website</a>
<?php endif; ?>

<?php if(isset($fields["field_resource_document"]->content)): ?>
	<a class="btn btn-primary" href="<?php print $fields["field_resource_document"]->content; ?>" role="button">Download <?php print $fields["title"]->content; ?></a>
<?php endif; ?>