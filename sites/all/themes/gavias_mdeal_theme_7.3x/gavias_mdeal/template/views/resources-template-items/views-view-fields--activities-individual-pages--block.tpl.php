<?php if(isset($fields["body"]->content)): ?>
<div class="activity-glossary-item__description">
	<?php print $fields["body"]->content; ?>
</div>
<?php endif; ?>

<?php if(isset($fields["field_activity_link"]->content)): ?>
	<div class="activity-glossary-item__link">
		<div class="activity-glossary-item__link-label"><?php print $fields["field_activity_link"]->label; ?></div>
		<?php print $fields["field_activity_link"]->content; ?>
	</div>
<?php endif; ?>