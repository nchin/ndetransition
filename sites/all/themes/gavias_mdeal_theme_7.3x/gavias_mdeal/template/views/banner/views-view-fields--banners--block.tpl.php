<div class="hero-container">
	<picture class="hero-banner-image">
		<source 
			media="(max-width: 599px)"
			srcset="<?php print $fields["field_banner_image_mobile_1x"]->content; ?> 1x, 
						 	<?php print $fields["field_banner_image_mobile_2x"]->content; ?> 2x" >
		<source 
			media="(min-width: 600px) and (max-width: 1023px)"
			srcset="<?php print $fields["field_banner_image_tablet_1x"]->content; ?> 1x, 
							<?php print $fields["field_banner_image_tablet_2x"]->content; ?> 2x" >
		<source
			media="(min-width: 1024px)"
			srcset="<?php print $fields["field_banner_image_wide_1x"]->content; ?> 1x, 
							<?php print $fields["field_banner_image_wide_2x"]->content; ?> 2x">
		<img 
			src="<?php print $fields["field_banner_image_wide_1x"]->content; ?>"
			alt="<?php print $fields["field_banner_image_alt_text"]->content; ?>"
			sizes="100vw" 
			srcset="<?php print $fields["field_banner_image_mobile_1x"]->content; ?> 600w, 
							<?php print $fields["field_banner_image_tablet_1x"]->content; ?> 1024w,
							<?php print $fields["field_banner_image_wide_1x"]->content; ?> 1680w "  >
	</picture>

	<div class="text-over-image">
			<div class="hero-banner-title"><?php print $fields["title"]->content; ?></div>
	</div>

</div>