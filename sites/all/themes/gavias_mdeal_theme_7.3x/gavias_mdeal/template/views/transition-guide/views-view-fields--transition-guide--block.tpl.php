<div class="panel panel-default">
	<div class="panel-heading collapsed" role="tab" id="heading-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($fields['name']->content))); ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($fields['name']->content))); ?>" aria-expanded="false" aria-controls="collapse-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($fields['name']->content))); ?>">
	      <h2 class="panel-title"><?php print $fields["name"]->content; ?></h2>
	</div>
	<div id="collapse-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($fields['name']->content))); ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($fields['name']->content))); ?>">
	  <div class="panel-body tg-section__body">
	    <div class="tg-section__body__section tg-section__question">
			<h3><?php print $fields["field_guide_question"]->label; ?></h3>
			<?php print $fields["field_guide_question"]->content; ?>
		</div>
		<div class="tg-section__body__section tg-section__activities">
			<h3><?php print $fields["view"]->label; ?></h3>
			<?php print $fields["view"]->content; ?>
		</div>
		<div class="tg-section__body__section tg-section__resources">
			<h3><?php print $fields["view_1"]->label; ?></h3>
			<?php print $fields["view_1"]->content; ?>
		</div>
	  </div>
	</div>
</div>