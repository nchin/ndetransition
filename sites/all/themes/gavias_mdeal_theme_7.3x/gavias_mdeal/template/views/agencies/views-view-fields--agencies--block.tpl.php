<a class="resource-item resource__link" href="<?php print $fields["field_resource_link"]->content; ?>" target="_blank">
	<span class="resource-item__title"><?php print $fields["title"]->content; ?></span>
    <span class="resource-item__desc"><?php print $fields["body"]->content; ?></span>
</a>