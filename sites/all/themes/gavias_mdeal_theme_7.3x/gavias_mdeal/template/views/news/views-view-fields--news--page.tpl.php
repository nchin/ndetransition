<a class="news-item news-item__page" href="<?php print $fields["path"]->content; ?>">	
	<h3 class="news-item__title"><?php print $fields["title"]->content; ?></h3> 
	<div class="news-item__pub-date"><span class="text-lowercase">posted on</span> <?php print $fields["created"]->content; ?></div>   
</a>