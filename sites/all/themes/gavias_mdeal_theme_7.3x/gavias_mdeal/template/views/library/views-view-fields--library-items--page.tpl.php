<a class="col-sm-6 col-md-4 card card--library-item" href="<?php print $fields["path"]->content; ?>" target="_blank">
	<div class="card__inner">
		<?php print $fields["field_product_image"]->content; ?>
		<h4 class="card__library-title"><?php print $fields["title"]->content; ?></h4>
	</div>
</a>