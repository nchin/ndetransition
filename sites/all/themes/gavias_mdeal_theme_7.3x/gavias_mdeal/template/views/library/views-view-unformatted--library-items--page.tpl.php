<section class="library-list-category">
	<?php if (!empty($title)): ?>
	  	<div class="library-list-category__title">
	  		<h3><?php print $title; ?></h3>
	  	</div>
	<?php endif; ?>

	<div class="library-list-category__items">
		<?php foreach ($rows as $id => $row): ?>
		    <?php print $row; ?>
		<?php endforeach; ?>
	</div>
</section>