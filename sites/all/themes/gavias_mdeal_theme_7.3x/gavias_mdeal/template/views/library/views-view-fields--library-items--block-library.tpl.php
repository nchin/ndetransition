<div class="card card--library-item col-md-2 col-sm-3 col-xs-4" href="<?php print $fields["path"]->content; ?>" target="_blank">
	<div class="card__inner">
		<?php if(isset($fields["field_product_image"]->content)): ?>
			<?php print $fields["field_product_image"]->content; ?>
		<?php endif; ?>
		<h4 class="card__library-title"><?php print $fields["title"]->content; ?></h4>
		<?php if(isset($fields["add_to_cart_form"]->content)): ?>
			<?php print $fields["add_to_cart_form"]->content; ?>
		<?php endif; ?>
	</div>
</div>