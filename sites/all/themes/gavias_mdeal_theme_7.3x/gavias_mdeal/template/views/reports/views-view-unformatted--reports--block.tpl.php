<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="reports">
	<?php if (!empty($title)): ?>
	  	<div class="reports__year">
	  		<h2><?php print $title; ?></h2>
	  	</div>
	<?php endif; ?>

	<div class="reports__list-wrapper">
		<?php foreach ($rows as $id => $row): ?>
		    <?php print $row; ?>
		<?php endforeach; ?>
	</div>
</div>