<a class="front-button__card" href="<?php print $fields["path"]->content; ?>">
	<div class="button__header">
		<?php print $fields["field_lp_landing_page_image_1"]->content; ?>		
	</div>	
	<div class="button__text">
		<h2><?php print $fields["title"]->content; ?></h2>
    	<p><?php print $fields["body"]->content; ?></p>
	</div>    
</a>
