<?php if (!empty($title)): ?>
  <h2 class="section-header text-center"><?php print $title; ?></h2>
<?php endif; ?>
<div class="front-button-flex-container">
	<?php foreach ($rows as $id => $row): ?>
	    <?php print $row; ?>
	<?php endforeach; ?>
</div>