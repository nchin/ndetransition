<div class="panel-body">
	<?php if(isset($fields["title"]->content)): ?>
		<div class="session-title">
			<?php print $fields["title"]->content; ?>
		</div>
	<?php endif; ?>

	<?php if(isset($fields["field_room"]->content)): ?>
		<span><strong><?php print $fields["field_room"]->label; ?></strong></span> <?php print $fields["field_room"]->content; ?>
	<?php endif; ?>

	<?php if(isset($fields["field_slides_file"]->content)): ?>
		<?php print $fields["field_slides_file"]->content; ?>
	<?php endif; ?>
</div>