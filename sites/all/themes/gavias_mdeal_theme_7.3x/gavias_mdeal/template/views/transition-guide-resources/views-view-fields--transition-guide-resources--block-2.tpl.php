<?php if(isset($fields["field_resource_document"]->content)): ?>
	<a class="resource-item resource__link tg-resource-item" href="<?php print $fields["field_resource_document"]->content; ?>" target="_blank">
<?php endif; ?>

<?php if(isset($fields["field_resource_link"]->content)): ?>
	<a class="resource-item resource__link tg-resource-item" href="<?php print $fields["field_resource_link"]->content; ?>" target="_blank">
<?php endif; ?>

	<span class="resource-item__title"><?php print $fields["title"]->content; ?></span>
    <span class="resource-item__desc"><?php print $fields["body"]->content; ?></span>
</a>