<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="heading-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($title))); ?>" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($title))); ?>" aria-expanded="false" aria-controls="collapse-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($title))); ?>">
	    
	      	<?php if (!empty($title)): ?>
			  	<h2 class="panel-title"><?php print $title; ?></h2>
			<?php endif; ?>
	</div>
	<div id="collapse-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($title))); ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php print strtolower(preg_replace('/\s/', '-', strip_tags($title))); ?>">
	  <div class="panel-body resource-list-wrapper tg-resource-list-wrapper">
	  	<!-- Panel content -->
	    <?php foreach ($rows as $id => $row): ?>
		    <?php print $row; ?>
		<?php endforeach; ?>
	  </div>
	</div>
</div>
</div>
