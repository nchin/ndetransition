<!--view view unformmated -->
<div class="resource-container">
	<?php if (!empty($title)): ?>
	  	<h2 class="text-center"><?php print $title; ?></h2>
	<?php endif; ?>

	<div class="resource-list-wrapper tg-resource-list-wrapper">
		<?php foreach ($rows as $id => $row): ?>
		    <?php print $row; ?>
		<?php endforeach; ?>
	</div>
</div>
