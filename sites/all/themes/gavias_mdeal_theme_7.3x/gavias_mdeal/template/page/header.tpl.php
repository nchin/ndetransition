<?php if(theme_get_setting('preloader') == '1'): ?>
  <div id="jpreContent" >
    <div id="jprecontent-inner">
        <div class="preloader-wrapper active">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div>
            <div class="gap-patch">
              <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
      </div>  
    </div>
  </div>
<?php endif; ?> 

<header id="header" <?php if(theme_get_setting('sticky_menu') == 1){echo 'class="gv-fixonscroll"';} ?>>

  <?php if (isset($page['branding'])) : ?>
     <?php print render($page['branding']); ?>
  <?php endif; ?>

  <div class="header-main">
     <div class="container">
        <div class="header-main-inner">
          <!-- This section gets pushed to the left side-->
          <div class="header-main-inner-section">
              <?php if ($logo): ?>
                <!--<h1 class="logo">-->
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="icon-logo">
                    <span><?php print $site_name; ?></span>
                    <!--<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>-->
                  </a>
                <!--</h1>-->
              <?php endif; ?>
          </div>
          <!-- This section gets pushed to the right side-->
          <div class="header-main-inner-section">
              <?php if ($menu_bar = render($page['main_menu'])): print $menu_bar; endif; ?>
              <!--<div class="icon-search"><span class="element-invisible">Show search form</span></div>-->           

              <?php if($page['search']){ ?>
                <div class="icon-search">
                  <span class="element-invisible">Show search form</span>
                </div>
                <div class="search-region">
                    <?php print render($page['search']); ?>
                </div>
              <?php } ?>  

          </div> 

        </div> 
      </div>     
  </div> 

</header>
