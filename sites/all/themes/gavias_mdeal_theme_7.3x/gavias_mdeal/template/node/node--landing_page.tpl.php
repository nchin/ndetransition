<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
	
	<?php if (!$page): ?>
   		<h1 class="element-invisible" <?php print $title_attributes; ?>><?php print $title; ?></h1>
  	<?php endif; ?>

	<?php print render($title_suffix); ?>

	<div class="content"<?php print $content_attributes; ?>>
	    <div class="intro-text"><?php  print render($content['body']); ?></div>
	</div>



</div>



