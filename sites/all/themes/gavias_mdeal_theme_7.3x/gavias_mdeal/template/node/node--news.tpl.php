<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php if ($display_submitted): ?>
    <div class="submitted news-item__pub-date">
      <?php print "posted on " . date( "F j, Y",$node->created); ?>
    </div>
  <?php endif; ?>

  <?php print render($title_prefix); ?>
    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
  <?php print render($title_suffix); ?>

  

  <div class="news-content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
    ?>

    <div class="news__content-body">
      <div class=" <?php if (!empty($node->field_image)) {
                echo "news__content-body-partialwidth";
            } else {
                echo "news__content-body-fullwidth";
            } ?>">
        <?php  print render($content['body']); ?>
      </div>
      <?php if (!empty($node->field_image)): ?>
      <div class="news__content-image">
        <?php  print render($content['field_image']); ?>
      </div>
      <?php endif; ?>
    </div>
    
    <?php if (!empty($node->field_file_attachment)): ?>
    <div class="news__file-attachment-button">
      <?php $file = file_load($node->field_file_attachment['und'][0]['fid']); ?>

      <a class="btn btn-primary" href="<?php print file_create_url($file->uri); ?>">
        <?php print $node->field_file_attachment['und'][0]['description']; ?>
      </a>
    </div>
    <?php endif; ?>

  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</article>

