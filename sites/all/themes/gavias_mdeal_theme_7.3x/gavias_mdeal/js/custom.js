// Search button toggle
jQuery(document).ready(function($) {
	var searchIcon = $('.icon-search');
	var closeIcon = $('.icon-close');
	var searchForm = $('.search-region');
	var searchInput = $('#edit-search-block-form--2');
	var hamburgerContainer = $('.tb-megamenu button');
	var dropdownContainer = $('.tb-megamenu .nav-collapse')
	var isOpen = false;

	// close the main menu if it is open when search icon is clicked
	function closeMobileNav() {
		if (dropdownContainer.hasClass('in')) {
			dropdownContainer.addClass('collapse');
			dropdownContainer.removeClass('in');
			dropdownContainer.css('height', 0);
			dropdownContainer.css('overflow', 'hidden');
		};
	}

	// close search form if is open
	function closeSearch() {
		if (isOpen == true) {
			searchForm.removeClass('search-open');
			searchIcon.addClass('icon-search');
			searchIcon.removeClass('icon-close');
			searchInput.focusout();
			isOpen = false;
		};
	};

	// show or hide search form on click
	searchIcon.click(function() {

		closeMobileNav();

		if (isOpen == false) {
			searchForm.addClass('search-open');			
			searchIcon.removeClass('icon-search');
			searchIcon.addClass('icon-close');
			searchInput.focus();
			isOpen = true;
		} else {
			searchForm.removeClass('search-open');
			searchIcon.addClass('icon-search');
			searchIcon.removeClass('icon-close');
			searchInput.focusout();
			isOpen = false;
		};
	});

	// close search form when hamburger button is clicked
	hamburgerContainer.click(function() {
		closeSearch();
		console.log("wrong");
	});

	// hides the dropdown menu when user clicks anywhere outside it, except on button or on its own	
	dropdownContainer.mouseup(function () {
    	return false;
    });
    hamburgerContainer.mouseup(function () {
    	return false;
    });

    $(document).mouseup(function() {
		closeMobileNav();
    });
	
	// hides the search input field when user clicks anywhere outside it, except on button or on its own
    searchIcon.mouseup(function() {
		return false;
    });
    searchForm.mouseup(function() {
		return false;
    });
    
    $(document).mouseup(function() {
		if (isOpen == true) {
			searchForm.removeClass('search-open');
			searchIcon.click();
		}
    });


    // checks to see if user entered anything inside search field and if search field is not empty, set .searchForm-icon to display none	
	function buttonUp() {
	    var inputVal = searchInput.val();
	    // removes all the space from the value and save it back to inputVal
	    inputVal = $.trim(inputVal).length;
	    if (inputVal !== 0) {
			searchForm.removeClass('search-open');
	    } else {
			searchInput.val('');
			searchForm.addClass('search-open');
	    }
	}

	
	// run buttonUp function whenever searchbox-input is in focus
	//searchInput.keyup(buttonUp);

});