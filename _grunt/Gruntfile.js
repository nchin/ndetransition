/*
 After you have changed the settings at "Your code goes here",
 run this with one of these options:
  "grunt" alone creates a new, completed images directory, re-processes images, and watch for any new changes
  "grunt clean" removes the svg directory
  "grunt responsive_images" re-processes images to specified sizes
  "grunt icons" minimizes SVG files and make SVG icons into a CSS file
*/

module.exports = function(grunt) {
  // load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
  require('load-grunt-tasks')(grunt);

  // TASK CONFIGURATIONS
  grunt.initConfig({
    /* Generate the images directory if it is missing */
    mkdir: {
      dev: {
        options: {
          create: ['images/']
        },
      },
    },

    // re-processes images to specified sizes
    responsive_images: {
      dev: {
        options: {
          engine: 'im',
          sizes: [{
            name: 'wide',
            width: '1600',
            suffix: '_large',
            quality: 30
          },{
            name: 'normal',
            width: '800',
            suffix: '_normal',
            quality: 40
          }]
        },

        /*
        You don't need to change this part if you don't change
        the directory structure.
        */
        files: [{
          expand: true,
          src: ['*.{gif,jpg,png}'],
          cwd: 'images_src/',
          dest: 'images/'
        }]
      }
    },

    /* Copy the "fixed" images that don't go through processing into the images/directory */
    copy: {
      myicons: {
        files: [{
          expand: true,
          cwd: 'svg/output/',
          src: ['png/**/*','*.css'],
          dest: '../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/css'
        }]
      },
      script: {
        files: [{
          expand: true,
          cwd: 'svg/output/',
          src: ['grunticon.loader.js'],
          dest: '../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/js'
        }]
      }
    },

    /* Clear out the images directory if it exists */
    clean: {
      dev: {
        src: ['svg/output','svg/optimized'],
        options: {force: true}
      },
    },
    
    // minimize SVG files
    svgmin: {
      options: {
        plugins: [
          { removeXMLProcInst:false },  // prevent the XML header from being stripped
          { removeViewBox: false },
          { removeUselessStrokeAndFill: true },
        ]
      },
      dist: {
        expand:true,
        cwd: 'svg/raw',
        src: ['*.svg'],
        dest: 'svg/compressed',
        ext: '.colors-dark-bright-clean.svg'
      }
    },

    // make SVG icons into a CSS file
    grunticon: {
      myIcons: {
        files: [{
          expand: true,
          cwd: 'svg/compressed',
          src: ['*.svg', '*.png'],
          dest: "svg/output"
        }],
        options: {
          colors: {
              dark: '#333',
              bright: '#a0a0a0',
              clean: '#fff'
          },
          enhanceSVG: true,
          customselectors: {
                //lets you define custom class name for where you want to reference the icons in a specific situation e.g. pseudo class 
                // 'icon-name' : [".custom-class-name"]
                // $1 references the filename
                "close" : [".icon-search:focus"],
                'arrow-down-dark' : [".iep-step:nth-last-child(n+2):after"],
                'infomation-circle' : [".agency_guide:before"],
                'arrow-right-bright' : [".resource__link:after"],
                'download-dark' : [".reports__item:after"],
                'arrow-down-bright' : [".panel-heading.collapsed:after"],
                'arrow-up-bright' : [".panel-heading:after"],
                'file-pdf-dark' : ["a[href$='.pdf']:before"],
                'arrow-left' : [".btn-library:before"],
                //'signup-line-fire' : [".icon-signup-line-dark:hover"],
                //'team-filled-fire' : [".icon-team-filled-dark:hover"],
                //'user-group-filled-fire' : [".icon-user-group-filled-dark:hover"]
          }
        }
      }
    },

    // Compile SASS file into CSS
    compass: {
      dist: {
        options: {
          sassDir: ['../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/sass/'],
          cssDir: ['../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/css/'],
          environment: 'development',
          require: 'bootstrap-sass',
          sourcemap: true
        }
      }
    },

    postcss: {
      options: {
        processors: [
            require('pixrem')(), // add fallbacks for rem units
            require('autoprefixer')({browsers: 'last 5 versions'}), // add vendor prefixes
            //require('cssnano')() // minify the result
        ]
      },
      dist: { 
          src: '../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/css/*.css'
      }
    },

    uglify: {
      my_target: {
          files: {
              cwd: '../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/js',
              src: '**/*.js',
              dest: '../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/js'
          }
      }
    },

    // Watch these files and notify of changes.
    watch: {
      grunt: { files: ['Gruntfile.js'] },

      compass: {
        files: ['../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/sass/**/*.scss'],
        tasks: ['compass:dist', 'postcss']

      },
      js: {
        files: ['../sites/all/themes/gavias_mdeal_theme_7.3x/gavias_mdeal/js/**/*.js'],
        tasks: ['uglify']
        }
    },

  });
  

  // Establish tasks we can run from the terminal.
  grunt.registerTask('icons', ['clean', 'svgmin', 'grunticon', 'copy']);
  grunt.registerTask('images', ['mkdir', 'copy', 'responsive_images']);

};
